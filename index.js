
const vorpal = require('vorpal')()
const fs = require('fs')
const Constant = require('./src/util/constant')
const generateTranslationFile = require('./sync-spreadsheet.js').generateTranslationFile

let curGoogleSheetConfig = {}
let curPhraseAppSecret = {}
let config = {}

/**
 * This function check the google spreadsheet id and 
 * throw error if no client_id and client_secret are provided.
 * @param {*} opt 
 */
function chkGooleSheetId(vorpal, opt, config) {
  return new Promise((resolve, reject) => {
    if (!opt.gsheet_id) {
      vorpal.prompt({
        type: 'input',
        name: 'gsheetId',
        message: 'What is the source google sheet id? '
      }, (result) => {
        if (result.gsheetId) {
          config.gsheetId = result.gsheetId
          console.log(`Config new sheetId ${config.gsheet_id}`)
          return resolve(config)
        } else {
          return reject("Undefined spreadsheet id")
        }
      })
    } else {
      config.gsheetId = opt.gsheetId
      console.log(`Config new sheetId ${config.gsheet_id}`)
      return resolve(config)
    }
  })
}

function chkPhraseAppPrjId(vorpal, opt, config) {
  return new Promise((resolve, reject) => {
    if (!opt.phraseappPrjId) {
      vorpal.prompt({
        type: 'input',
        name: 'phraseappPrjId',
        message: 'What is the phrase app project id? '
      }, (result) => {
        if (result.phraseappPrjId) {
          config.phraseappPrjId = result.phraseappPrjId
          console.log(`Config new phraseApp project id ${config.phraseappPrjId}`)
          return resolve(config)
        } else {
          return reject("Undefined phraseapp id")
        }
      })
    } else {
      config.phraseappPrjId = opt.phraseappPrjId
      console.log(`Config new phraseapp project id ${config.phraseappPrjId}`)
      return resolve(config)
    }
  })
}

vorpal
  .command('config')
  .option('-g, --gsheet_id', "id of your google spreadsheet")
  .option('-pp, --phraseapp_project_id', "id of your phrase project")
  .description('config all the needed information to sync the i18n data')
  .action(function (opt, cb) {
    let vorpal = this
    chkGooleSheetId(vorpal, opt, config)
      .then(() => {
        return chkPhraseAppPrjId(vorpal, opt, config)
      }).then (() => {
        console.log(config)
        fs.writeFileSync(Constant.CONFIG_PATH, JSON.stringify(config), 'utf8')
      }).catch((e) => {
        return console.error('config error')
      })
  })

vorpal
  .command('build')
  .description('build the script with existent configuration')
  .action(function (opt, cb) {
    let tokenConfig
    try {
      config = JSON.parse(fs.readFileSync(Constant.CONFIG_PATH, 'utf8'))
    } catch(e) {
      return cb(e)
    }
    return generateTranslationFile(config.gsheetId, (err) => {
      if (err) {
        console.error('error in syncing spreadsheet')
      }
      console.log('Done the transformations')
    })
  })



vorpal
  .delimiter('i18n-tool $')
  .show()
  .parse(process.argv)
