const Constant = require('./src/util/constant')

const DEST_FILE = Constant.GENERATED_DEST_PATH


const fs = require('fs')
const fso = require('fs-extra')

const GoogleDoc = require('./src/google-doc')
const I18nFormatter = require('./src/i18n-formatter')
const BBird = require('bluebird')
const js2xmlparser = require('js2xmlparser')
const PhraseApp = require('./src/phraseapp-client')

// This function is not well tested because phrase app is going to be deprecated 
// due to the stupid price.
async function syncSpreadSheetToPhraseApp(sheetId, phraseappPrjId, phraseappToken, cb) {
  const phraseAppClient = new PhraseApp (phraseappToken, phraseappPrjId)
  const formatter = parseGoogleSpreadSheet(sheetId)
  const KVYaml = formatter.getYaml()
  fs.writeFileSync(DEST_FILE, KVYaml, {
    encoding: 'utf8'
  })
  const remove = await phraseAppClient.deleteAllKeys()
  const data = await phraseAppClient.uploadFile(DEST_FILE)
  console.log(`Update project: ${phraseappPrjId}, from spreadsheet ${sheetId}, successfully`)
  console.log(data)
  return cb(null)
}

async function generateTranslationFile(sheetId, cb) {
  const formatter = await parseGoogleSpreadSheet(sheetId)
  const iosString = formatter.getiOSFormat()
  Object.keys(Constant.SUPPORTED_LANGUAGES).forEach( (key) =>{
    let locale = Constant.SUPPORTED_LANGUAGES[key]
    let path = `./translations/ios/${locale}.lproj/Localizable.strings`
    fso.outputFileSync(path, iosString[locale], {
      encoding: 'utf8'
    })
  })
  const androidXML = formatter.getAndroidXML()
  console.log();
  Object.keys(Constant.SUPPORTED_LANGUAGES).forEach( (key) =>{
    let locale = Constant.SUPPORTED_LANGUAGES[key]
    let path = `./translations/android/values-${locale}/strings.xml`
    let xmlResult = js2xmlparser.parse("resources", androidXML[locale])
    xmlResult = xmlResult.replace(/<\?xml version=\'1.0\'\?>/, "<?xml version=\'1.0\' encoding=\'UTF-8\'?>")
    fso.outputFileSync(path, xmlResult, {
       encoding: 'utf8'
     })
  })

  return cb(null)
}


async function parseGoogleSpreadSheet(sheetId) {
  const secretF = JSON.parse(fs.readFileSync(Constant.GOOGLE_CLIENT_SECRET_PATH, 'utf8'))
  const googleDoc = new GoogleDoc(secretF.client_id, secretF.client_secret)
  BBird.promisifyAll(googleDoc)
  try {
    await googleDoc.getTokenAsync({})
  } catch (e) {
    return cb(e)
  }
  const spreadsheet = googleDoc.getSpreadsheet(sheetId)
  await spreadsheet.build()
  const formatter = new I18nFormatter(spreadsheet)
  formatter.build()
  return formatter
}


module.exports.syncSpreadSheetToPhraseApp = syncSpreadSheetToPhraseApp
module.exports.generateTranslationFile = generateTranslationFile
