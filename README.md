# i18n-tool

This project help
1. Fetch key value pairs from google sheet (follow the with sheet name as namespace), and build to a new file and upload to phraseApp.


# Configuration

1. Prepare `google-client-secret.json` and put in `./secret/google-client-secret.json`. Check this link if you are the first to deploy this serive [link](https://developers.google.com/identity/sign-in/web/sign-in).
1. Prepare `phraseapp-client-secret.json` and put in `./secret/phraseapp-client-secret.json`
  1. Get the project id thru phraseapp client
  2. The json file has 2 keys "project_id", "client_secret"
2. Run `npm run config` or `node index.js config` and follow the instruction to do the configuration of 
  1. "id of google spreadsheet" 
  2. "id of phraseApp project" 