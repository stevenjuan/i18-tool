let axios = require('axios')
let Buffer = require('buffer').Buffer
let fs = require('fs')
let FormData = require('form-data')
const DOMAIN = 'https://api.phraseapp.com/'


class PhraseAppClient {
  constructor(accessToken, projectId, logger) {
    this.accessToken = accessToken
    this.projectId = projectId
    this.logger = logger || console
  }
  async uploadFile(filePath) {
    let data = new FormData()
    data.append('file', fs.createReadStream(filePath))
    data.append('update_translations', 'true')
    let headers = data.getHeaders()
    this.setAuthToken(headers)

    const path = `${DOMAIN}api/v2/projects/${this.projectId}/uploads`
    const result = await axios.post(path, data, {
      headers
    })
    // check if the response is correct
    if (result.status === 201) {
      return result.data
    } else {
      throw 'Response Error'
    }
  }
  async deleteAllKeys() {
    let headers = {}
    this.setAuthToken(headers)
    const timeoutSec = 30

    const path = `${DOMAIN}api/v2/projects/${this.projectId}/keys`
    this.logger.info('Start to delete all keys')
    const startTime = new Date()
    let intervalObj = setInterval(() => {
      this.logger.info('.')
    }, 1000)
    try {
      const result = await axios.delete(path, {
        headers,
        timeout: timeoutSec * 1000, //set a longer time out to wait if the delete is done
      })
      clearInterval(intervalObj)
      const endTime = new Date()
      this.logger.info(`Successfully delete all keys in ${(endTime - startTime) / 1000} seconds`)
      if (result.status === 200) {
        this.logger.info(result.data)
      }
      return result
    } catch (e) {
      clearInterval(intervalObj)
      this.logger.error(`Request timeout after ${timeoutSec} seconds`)
      throw e
    }
  }

  setAuthToken(headers) {
    const token = Buffer.from(this.accessToken, 'utf8').toString('base64')
    headers.Authorization = `Basic ${new Buffer(this.accessToken).toString('base64')}`
  }
}
module.exports = PhraseAppClient