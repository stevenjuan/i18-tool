const {google} = require('googleapis')
const BBird = require('bluebird')

class GoogleSheet {
  constructor(id, authClient) {
    this.authClient = authClient
    this.spreadsheetId = id
    this.gSheet = google.sheets('v4')
    BBird.promisifyAll(this.gSheet.spreadsheets)
    BBird.promisifyAll(this.gSheet.spreadsheets.values)
  }
  async build() {
    this.spreadsheet = await this.gSheet.spreadsheets.getAsync({
      auth: this.authClient,
      spreadsheetId: this.spreadsheetId,
    })
    this.sheets = {}
    // build sheets
    await this._buildAllSheets(this.spreadsheet.data.sheets)
    return
  }
  async _buildAllSheets(sheets) {
    const ranges = sheets.map((sheet) => {
      return `${sheet.properties.title}!A1:Z${sheet.properties.gridProperties.rowCount}`
    })
    let sheetsValueResp = await this.gSheet.spreadsheets.values.batchGetAsync({
      auth: this.authClient,
      spreadsheetId: this.spreadsheetId,
      ranges,
    })
    let sheetsValue =  sheetsValueResp.data


    // Google API Guarantee the order is the same
    for (let i in sheets) {
      this.sheets[sheets[i].properties.title] = {}
      this.sheets[sheets[i].properties.title].values = sheetsValue.valueRanges[i].values
      this.sheets[sheets[i].properties.title].range = sheetsValue.valueRanges[i].range
    }
    return
  }
}
module.exports = GoogleSheet