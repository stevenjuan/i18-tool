const jsYaml = require('js-yaml')
const _ = require('lodash')
const Constant = require('./util/constant.js')

const indexToLang = Constant.SUPPORTED_LANGUAGES
const SUPPORTED_LANG_NUMS = Object.keys(indexToLang).length

//Read and create a new format
class I18nFormatter {
  constructor(spreadsheet) {
    this.spreadsheet = spreadsheet

    // TODO if sheets is undefined, error should be throw
    this.sheets = spreadsheet.sheets

    // Hardcode languages
    this.i18nKV = {}
  }

  build() {
    let pageNames = Object.keys(this.sheets)
    for (let pageName of pageNames) {
      let vals = this.sheets[pageName].values
      for (let i in vals) {
        if (i === '0') {
          continue
        }
        let key = vals[i][0]
        if (_.isNil(key)) {
          console.warn(`Value in page:${pageName} of zh-TW: ${vals[i][1]}, en: ${vals[i][2]}, has no key`)
          continue
        } else if (key === '') {
          console.warn(`empty string key in page: ${pageName}`)
          console.warn(vals[i])
          continue
        }
        for (let j = 1; j < SUPPORTED_LANG_NUMS + 1; j++) {
          if (this.i18nKV[indexToLang[j]] === undefined) {
            this.i18nKV[indexToLang[j]] = {}
          }
          if (vals[i][j]) {
            this._AddNameSpace(this.i18nKV[indexToLang[j]], pageName, key, vals[i][j])
          } else {
            console.warn(`Lang: ${indexToLang[j]}, Page: ${pageName}, Key: ${key} has no value `)
          }
        }
      }
    }
  }

  /**
   * 
   * @param {string} kvOfLang   the key-value object of specific language
   * @param {string} pageName   the page name of the language
   * @param {string} key 
   * @param {string} val        value
   */
  _AddNameSpace(kvOfLang, pageName, key, val) {
    if (kvOfLang[pageName] === undefined) {
      kvOfLang[pageName] = {}
    }
    kvOfLang[pageName][key] = val
  }

  _detectDuplication() {

  }

  _detectInconsistent() {

  }

  _replacePlaceHolder() {

  }

  getYaml() {
    this.i18nKVYaml = jsYaml.dump(this.i18nKV)
    return this.i18nKVYaml
  }

  getiOSFormat() {
    const iOSLocaleString = {}
    function recursiveiOSstring(object, prefixStr = '' ,result = '') {
      if (typeof object === 'string') {
        const newPrefixStr = prefixStr.replace (/\.$/, '') //remove the trailing dot
        const line = `\"${newPrefixStr}\" = \"${object}\" \n\n`
        result = result + line
        return result
      } else if (typeof object === 'object') {
        Object.keys(object).forEach((key) => {
          const newPrefixStr = `${prefixStr}${key}.`
          result = recursiveiOSstring(object[key], newPrefixStr, result)
        })
        return result 
      } else {
        console.error('Error: in iOS string generation. Exiting...')
        return process.exit(1)
      }
    }
    Object.keys(this.i18nKV).forEach((locale) => {
      if (!this.i18nKV.hasOwnProperty(locale)) {
        return;
      }
      let content = this.i18nKV[locale]
      let result = recursiveiOSstring(content)
      iOSLocaleString[locale] = result
    })
    return iOSLocaleString
  }
  getAndroidXML () {
    const AndroidJson = {}
    function recursiveAndroidXML(object, prefixStr, resultAry) {
      if (typeof object === 'string') {
        const key = prefixStr.replace (/_$/, '') //remove the trailing _
        const val = object
        resultAry.push({
          '@': {
            'name': key
          },
          '#': object
        })
        return resultAry
      } else if (typeof object === 'object') {
        Object.keys(object).forEach((key) => {
          const newPrefixStr = `${prefixStr}${key}_`
          resultAry = recursiveAndroidXML(object[key], newPrefixStr, resultAry)
          return
        })
        return  resultAry
      } else {
        console.error('Error: in Android XML generation. Exiting...')
        return process.exit(1)
      }
    }
    Object.keys(this.i18nKV).forEach((locale) => {
      if (!this.i18nKV.hasOwnProperty(locale)) {
        return;
      }
      let content = this.i18nKV[locale]
      AndroidJson[locale] = {
        'string':[]
      }
      recursiveAndroidXML(content, '', AndroidJson[locale].string)
    })
    return AndroidJson
  }
}
module.exports = I18nFormatter