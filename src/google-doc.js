/**
 * google doc is formally calls google documents, which includes google sheet, document ...etc
 * This class is in charge of handling all the interaction with google docs
 */
const { OAuth2Client } = require('google-auth-library')
const Constant = require('./util/constant')
const fs = require('fs')
const readline = require('readline')

const GoogleSheet = require('./google-sheet')

const SCOPES = ['https://www.googleapis.com/auth/spreadsheets.readonly', 'https://www.googleapis.com/auth/spreadsheets']

// redirect uri currently is redundant but mandatory for now
const REDIRECT_URIS = 'urn:ietf:wg:oauth:2.0:oob'

const CREDENTIALS_PATH = Constant.GOOGLE_CREDENTIALS_PATH // The working path is in project root, which node process running

function storeCredentials(credentialsPath, credentials) {
  fs.writeFileSync(credentialsPath, JSON.stringify(credentials))
  console.log(`credentials stored to  + ${CREDENTIALS_PATH}`)
}

/**
 * This class handles google document's client authorization. Only set the client id and client secret from google project.
 */
class GoogleDoc {

  constructor(clientId, clientSecret, logger) {
    this.clientId = clientId
    this.clientSecret = clientSecret
    this.oauth2Client = new OAuth2Client(this.clientId, this.clientSecret, REDIRECT_URIS)
    this.logger = logger || console
  }

  getToken(options, callback) {
    const opt = options || {}
    const credentialsPath = opt.credentialsStorePath || CREDENTIALS_PATH
    const setCredentials = (err, credentials) => {
      if (err) {
        return callback(err)
      }
      this.oauth2Client.credentials = credentials
      storeCredentials(credentialsPath, credentials)
      return callback(null, credentials)
    }
    try {
      const credentials = JSON.parse(fs.readFileSync(credentialsPath, 'utf8'))
      this.logger.log('refreshing')
      this.logger.log('old token: ')
      this.logger.log(credentials)
      return this.refreshToken(credentials, setCredentials)
    } catch (err) { // if old credentials inexistent
      this.logger.log("No existent credentials")
      return this.getNewToken(credentialsPath, setCredentials)
    }
  }

  refreshToken(credentials, callback) {
    this.oauth2Client.credentials = credentials
    this.oauth2Client.refreshAccessToken((err, newCredentials) => {
      if (err) {
        return callback(err)
      }
      this.oauth2Client.credentials = newCredentials
      return callback(null, credentials)
    })
  }

  getNewToken(credentialsPath, callback) {
    const authUrl = this.oauth2Client.generateAuthUrl({
      access_type: 'offline',
      scope: SCOPES,
    })
    console.log('Authorize this app by visiting this url: ', authUrl)
    const rl = readline.createInterface({
      input: process.stdin,
      output: process.stdout,
    })
    rl.question('Enter the code from that page here: ', (code) => {
      rl.close()
      this.oauth2Client.getToken(code, (err, credentials) => {
        if (err) {
          this.logger.log('Error while trying to retrieve access credentials', err)
          return callback(err)
        }
        this.oauth2Client.credentials = credentials
        return callback(null, credentials)
      })
    })
  }

  getSpreadsheet(sheetId) {
    return new GoogleSheet(sheetId, this.oauth2Client)
  }
}

module.exports = GoogleDoc
