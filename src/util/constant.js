module.exports = {
  GOOGLE_CREDENTIALS_PATH: './secret/google-access-credentials.json',
  GOOGLE_CLIENT_SECRET_PATH: './secret/google-client-secret.json', // This file is download from google project
  CONFIG_PATH: './secret/generated-config.json',
  PHRASEAPP_CLIENT_SECRET_PATH: './secret/phrase-app-client-secret.json',
  GENERATED_DEST_PATH: './generated-i18n.yml',
  SUPPORTED_LANGUAGES: {
    '1': 'en',
    '2': 'id-ID',
    '3': 'vi-VN',
    '4': 'zh',
  }
}